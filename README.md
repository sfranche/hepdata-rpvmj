# HEPData-rpvmj

Collection of raw files and steering configurations to produce [HEPData](https://www.hepdata.net/),
using the [SUSY HEPData maker](https://gitlab.cern.ch/fgravili/susyhepdata-maker) tool.

## How to run

First it an installation of the `SUSY HEPData maker` tool is needed.
The esiest way is to use a docker image:
```
docker run --rm -v $PWD:/home/susyhep/HEPDataSubmission -it gitlab-registry.cern.ch/fgravili/susyhepdata-maker:master /bin/bash
```

Once installed, the command `hepdata_maker` should available.
Check the [documentation])(https://gitlab.cern.ch/fgravili/susyhepdata-maker/-/wikis/home) for all sub-commands and examples. To prepare a submission with current `steering` config, run:
```
hepdata_maker create-submission main_steering_file.json
```